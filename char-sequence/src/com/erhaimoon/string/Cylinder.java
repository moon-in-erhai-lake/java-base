package com.erhaimoon.string;

public class Cylinder {
	private double redius;
	private double height;
	//计算表面积2πr²+2πrh
	public void squaring() {
		double area=0;
		area=2*Math.PI*Math.pow(redius, 2)*height;
		System.out.println("圆柱体表面积："+area);
	}
	//计算体积V=Sh=πr²h
	public void cube() { 
		double v=0;
		v=Math.PI*Math.pow(redius, 2)*height;
		System.out.println("圆柱体体积："+v);
	}
	public static void main(String[] args) {
		Cylinder c = new Cylinder();
		c.redius=20;
		c.height=20;
		c.squaring();
		c.cube(); 
	} 

}
