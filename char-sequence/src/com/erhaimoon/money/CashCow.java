package com.erhaimoon.money;

public class CashCow {
	private int height;
	private int goldPiece;
	private boolean wormy;
	private int pesticide;
	
	//成长
	public void grow() {
		height+=2;
		goldPiece+=10;

	}
	//购买杀虫剂
	public void buy() {
		if (goldPiece>0) {
			goldPiece-=1;
			pesticide+=100;
		}

	}
	//杀死树上的虫子
	public void kill() {
		if(wormy) {
			pesticide-=50;
			goldPiece+=5;
			wormy=false;
		}

	}
	//浇水
	public void watering() {
		goldPiece+=5;
		height+=1;
		wormy=true;

	}
	public void show() {
		System.out.println("摇钱树的高度:"+height);
		System.out.println("摇钱树上的金币数量："+goldPiece);
		if(wormy) {
			System.out.println("树上有虫子");
		}else {
			System.out.println("树上没有虫子");
		}
		if (pesticide>0) {
			System.out.println("杀虫剂数量："+pesticide);
		}else {
			System.out.println("杀虫剂没有了");
		}
		
		

	}

}
