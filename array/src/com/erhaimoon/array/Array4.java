package com.erhaimoon.array;

public class Array4 {
	public static void main(String[] args) {
		char[] heavenlyStems = { '甲', '乙', '丙', '丁', '戊', '己', '庚', '辛', '壬', '癸' };

		char[] earthlyBranches = { '子', '丑', '寅', '卯', '辰', '巳', '午', '未', '申', '酉', '戌', '亥' };

		// 输出六十甲子名称 ( 要求每行输出12个，总共输出 5 行 )
		for (int i = 1; i <= 60; i++) {
			int j = i - 1;
			//取模求出索引
			int stemIndex = j % heavenlyStems.length;
			char c = heavenlyStems[stemIndex];
			int branchesIndex = j % earthlyBranches.length;
			char d = earthlyBranches[branchesIndex];
			System.out.print(c + "" + d + (i % 12 == 0 ? "\n" : "\t"));
		}
	}

}
