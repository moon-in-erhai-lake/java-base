package com.erhaimoon.array;

public class Array1 {
	public static void main(String[] args) {
		char[][] chunxiao = { 
				{ '春', '眠', '不', '觉', '晓' }, 
				{ '处', '处', '闻', '啼', '鸟' }, 
				{ '夜', '来', '风', '雨', '声' },
				{ '花', '落', '知', '多', '少' } };
		//先确定索引,后根据索引遍历数组
		for( int x = 0 ; x < 5 ; x++ ) { // 0 , 1 , 2 , 3 , 4 
			for( int y = 3 ; y >= 0 ; y-- ) { // 3 , 2 , 1 , 0
				char ch = chunxiao[ y ][x ] ;
				System.out.print( ch );
				System.out.print( y == 0 ? "\n" : "\t" );
			}
		}
	}
}
