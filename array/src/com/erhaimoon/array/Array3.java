package com.erhaimoon.array;

import java.util.Objects;

public class Array3 {
	public static void main( String[] args ) {
		int[] first = { 1, 3, 5 , 7 , 9 };
	  int[] second = { 1, 3, 5 , 9 , 7 };
	  int[] third = { 1, 3, 5 , 7 , 9 };
	  int[] fourth = { 1, 3, 5 , 7 };
	  
	  boolean x = equal( first , second );
	  System.out.println( x ); // false
	  
	  x = equal( first , third );
	  System.out.println( x ); // true
	  
	  x = equal( first , fourth );
	  System.out.println( x ); // false

	}
	public static boolean equal( int[] first , int[] second ) {
	    if (Objects.isNull(first)||Objects.isNull(second)) {//先判断是否为空
			System.out.println("指定数组");
		}
	    if (first.length!=second.length) {//再判断长度是否相等
			return false;
		}
	    for (int i = 0; i < second.length; i++) {//最后判断数组中的元素是否相等
			if (first[i]!=second[i]) {
				return false;
			}
		}
	    return true;
	}

}
