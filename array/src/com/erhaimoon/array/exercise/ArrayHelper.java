package com.erhaimoon.array.exercise;

/**
 * @program: java-base
 * @description: 比较数组-方法重载
 * @author: erhaimoon
 * @create: 2020-12-24 19:26
 **/
public class ArrayHelper {
    public static boolean equal(int[] first, int[] second) {
        //比较数组长度
        if (first.length != second.length) {
            return false;
        }
        //比较数组元素
        for (int i = 0; i < first.length; i++) {
            if (first[i] != second[i]) {
                return false;
            }
        }
        return true;
    }

    public static boolean equal(char[] first, char[] second) {
        if (first.length != second.length) {
            return false;
        }
        for (int i = 0; i < first.length; i++) {
            if (first[i] != second[i]) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int nums1[] = {4, 3, 2, 5, 48};
        int nums2[] = {4, 3, 3, 5};
        System.out.println(equal(nums1, nums2));
        char character1[] = {'兰', '州', '文', '理', '学', '院'};
        char character2[] = {'兰', '州', '文', '理', '学', '院'};
        System.out.println(equal(character1, character2));
    }

}
