package com.erhaimoon.array;

public class Array2 {
	public static void main(String[] args) {
		String[] name = { "雷军", "码云", "动摇", "谭浩强", "蓝馨仪" };
		String[] courses = { "C++", "java", "English" };
		int[][] scores = { { 90, 89, 75 }, { 59, 40, 100 }, { 100, 99, 80 }, { 80, 61, 61 }, { 60, 100, 99 } };
		orderByEnglish(name, scores);
		show(name, courses, scores);
		System.out.println("------------------------");
		orderByTotal(name, scores);
		System.out.println("------------------------");
		show(name, courses, scores); 

	}

	// 对总成绩排序
	public static void orderByTotal(String[] name, int[][] scores) {
		for (int i = 0; i < scores.length - 1; i++) {
			for (int j = 0; j < scores.length - 1 - i; j++) {
				int[] before = scores[j];
				int[] after = scores[j + 1];
				int f = sum(before);
				int s = sum(after);
				if (f < s) {
					int[] temp = scores[j];
					scores[j] = scores[j + 1];
					scores[j + 1] = temp;

					String x = name[j];
					name[j] = name[j + 1];
					name[j + 1] = x;
				}
			}
		}
	}

	// 对数组中每行的元素求和
	public static int sum(int[] scores) {
		int total = 0;
		for (int i = 0; i < scores.length; i++) {
			total += scores[i];
		}
		return total;
	}
	//按照英语成绩排序
	public static void orderByEnglish(String[] name, int[][] scores) {
		for (int i = 0; i < scores.length - 1; i++) {
			for (int j = 0; j < scores.length - 1 - i; j++) {
				int f = scores[j][2];
				int s = scores[j + 1][2];
				if (f < s) {
					int[] temp = scores[j];
					scores[j] = scores[j + 1];
					scores[j + 1] = temp;

					String x = name[j];
					name[j] = name[j + 1];
					name[j + 1] = x;
				}
			}
		}
	}
	//遍历数组
	public static void show(String[] name, String[] courses, int[][] scores) {
		for (int i = 0; i < scores.length; i++) {
			System.out.print(name[i] + "=>");
			for (int j = 0; j < scores[i].length; j++) {
				System.out.print(courses[j] + ":");
				System.out.print(scores[i][j]);
				if (j < scores[i].length - 1) {
					System.out.print(",");
				}
			}
			System.out.println();
		}
	}

}
