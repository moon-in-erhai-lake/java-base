package com.erhaimoon.classes.exercise;

import java.util.Objects;

public class Sorter {
	public void traversal(int[] array) { 
		if (Objects.isNull(array)) {
			System.out.println("array为空");
			return; 
		}
		if (array.length == 0) {
			System.out.println("array是个空数组");
			return;
		}
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + (i == array.length - 1 ? "\t" : ","));
		}
	}

	public void sort(int[] array) {
		int temp = 0;
		for (int i = 0; i < array.length - 1; i++) {
			for (int j = 0; j < array.length - i - 1; j++) {
				if (array[j] < array[j + 1]) {
					temp = array[j];
					array[j] = array[j + 1];
					array[j + 1] = temp;
				}
			}
		}
	}

	public static void main(String[] args) {
//		int [] a= null;
//		int [] a=new int[0];
		int[] a = { 1, 100, -20, 99, 1000, 0, 30 };
		Sorter sorter = new Sorter();
		System.out.print("排序前：");
		sorter.traversal(a);

		sorter.sort(a);
		System.out.print("排序后：");
		sorter.traversal(a);

	}
}
