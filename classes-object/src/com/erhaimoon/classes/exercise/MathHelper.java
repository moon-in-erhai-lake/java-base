package com.erhaimoon.classes.exercise;

/**
 * @program: java-base
 * @description: 辅助完成数学运算有关的操作
 * @author: erhaimoon
 * @create: 2020-12-24 19:57
 **/
public class MathHelper {
    //获取当前类的实例
    public static MathHelper getInstance() {
        MathHelper mathHelper = new MathHelper();
        return mathHelper;
    }

    public long max(long first, long... values) {
        long max = values[0];
        for (int i = 1; i < values.length; i++) {
            if (max < values[i]) {
                max = values[i];
            }
        }
        return max;
    }

    public double max(double first, double... values) {
        double max = values[0];
        for (int i = 1; i < values.length; i++) {
            if (max < values[i]) {
                max = values[i];
            }
        }
        return max;
    }

    public long min(long first, long... values) {
        long min = values[0];
        for (int i = 1; i < values.length; i++) {
            if (min > values[i]) {
                min = values[i];
            }
        }
        return min;
    }

    public double min(double first, double... values) {
        double min = values[0];
        for (int i = 1; i < values.length; i++) {
            if (min > values[i]) {
                min = values[i];
            }
        }
        return min;
    }
}
