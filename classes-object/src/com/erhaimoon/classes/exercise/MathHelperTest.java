package com.erhaimoon.classes.exercise;

/**
 * @program: java-base
 * @description: 测试类
 * @author: erhaimoon
 * @create: 2020-12-24 20:02
 **/
public class MathHelperTest {
    public static void main(String[] args) {
        //获取数学辅助类的实例
        MathHelper mh = MathHelper.getInstance();
        long n = 99L;
        short s = 77;
        byte b = 66;
        char c = 'x';
        //调用求最大值的方法
        long x = mh.max(n, s, b, c);
        System.out.println(x);
        //调用求最小值的方法
        long y = mh.min(n, s, b, c);
        System.out.println(y);
    }
}
