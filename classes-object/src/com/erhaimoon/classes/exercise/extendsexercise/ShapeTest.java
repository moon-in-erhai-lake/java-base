package com.erhaimoon.classes.exercise.extendsexercise;

/**
 * @program: java-base
 * @description: 测试类
 * @author: erhaimoon
 * @create: 2020-12-28 22:46
 **/
public class ShapeTest {
    public static void main(String[] args) {
        Triangle t = new Triangle("三角形");
        t.firstEdge=44;
        t.secondEdge=49;
        t.thirdEdge=52;
        t.calculate();//计算面积
        t.description();
        Circle c = new Circle("圆形");
        c.radius=8;
        c.calculate();
        c.description();
    }
}

