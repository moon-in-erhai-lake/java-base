package com.erhaimoon.classes.exercise.extendsexercise;

/**
 * @program: java-base
 * @description:
 * @author: erhaimoon
 * @create: 2020-12-28 22:31
 **/
public class Triangle extends Shape {
    protected double firstEdge;
    protected double secondEdge;
    protected double thirdEdge;
    public Triangle(String type){
        //可以在这里我完成对type的初始化
        super(type);
    }

    public void calculate(){
        //在这里计算三角形的面积，并将面积存储到area变量中
        double p=(firstEdge+secondEdge+thirdEdge)/2;
        // area是从父类继承的、可见的实例变量
        area=Math.sqrt(p*(p-firstEdge)*(p-secondEdge)*(p-thirdEdge));
    }
    public void description(){
        //这里输出三角形基本信息（三边的长度）
//        this.type="三角形";
        System.out.println("第一条边长："+firstEdge);
        System.out.println("第二条边长："+secondEdge);
        System.out.println("第三条边长："+thirdEdge);
        //最后通过调用从父类继承的、可见的show方法输出三角形的面积
        show();
    }

}
