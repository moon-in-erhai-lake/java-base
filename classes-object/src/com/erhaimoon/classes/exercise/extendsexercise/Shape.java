package com.erhaimoon.classes.exercise.extendsexercise;

/**
 * @program: java-base
 * @description:
 * @author: erhaimoon
 * @create: 2020-12-28 22:21
 **/
public class Shape {
    protected double area;//为了让子类能访问area变量，这里修饰符为protected
    protected String type;//为了让子类能够访问area变量，这里修饰符为protected

    public Shape( String type) {
        this.type = type;
    }

    public void show(){
        //在Shape类中show方法除了输出语句外，没有任何其它代码
        System.out.println(this.type+"面积为："+this.area);
    }
}
