package com.erhaimoon.classes.exercise.extendsexercise;

/**
 * @program: java-base
 * @description:
 * @author: erhaimoon
 * @create: 2020-12-29 08:15
 **/
public class Circle extends Shape {
    protected double radius;

    public Circle(String type) {
        super(type);
    }
    public void calculate(){
        area=Math.PI*Math.pow(radius,2);
    }
    public void description(){
        System.out.println("半径为："+radius);
        show();
    }
}
