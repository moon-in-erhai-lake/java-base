package com.erhaimoon.classes.exercise;

public class Sinaean {
	String name;
	char gender;
	int age;
	boolean married;

	public void marry(Sinaean another) {
		if (this.married == true || another.married == true) {
			System.out.println("有一个已婚，不可以结婚"); 
			return;
		}
		if (this.age<22||another.age<20) {
			System.out.println("年龄不符合要求，不能结婚");
			return;
		}
		if (this.gender == another.gender) {
			System.out.println("性别相同，不可以结婚");
			return;
		}
		System.out.println(this.name + "可以和" + another.name + "结婚");

	}

	public static void main(String[] args) {
		Sinaean s = new Sinaean();
		s.name = "自己";
		s.gender = '男';
		s.age = 22;
		s.married = false;
		Sinaean s1 = new Sinaean();
		s1.name = "小红";
		s1.gender = '女';
		s1.age = 20;
		s1.married = false;
		s.marry(s1);//谁调用this就是谁
	}

}
