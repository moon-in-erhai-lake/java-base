package com.erhaimoon.classes.polymorphicexercise;

/**
 * @program: java-base
 * @description: 熊类
 * @author: erhaimoon
 * @create: 2020-12-29 21:44
 **/
public class Bear extends Beast {
    public Bear(String type) {
        super(type);
    }
}
