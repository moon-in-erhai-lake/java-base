package com.erhaimoon.classes.polymorphicexercise;

/**
 * @program: java-base
 * @description: 鸟类
 * @author: erhaimoon
 * @create: 2020-12-29 21:33
 **/
public class Birds extends Animal {
    public Birds(String type) {
        super(type);
    }

    @Override
    public void eat(String food) {
        super.eat(food);
        System.out.println("直接用嘴啄食物");
    }
}
