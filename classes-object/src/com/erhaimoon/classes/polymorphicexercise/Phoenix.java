package com.erhaimoon.classes.polymorphicexercise;

/**
 * @program: java-base
 * @description: 鸟的子类
 * @author: erhaimoon
 * @create: 2020-12-29 21:41
 **/
public class Phoenix extends Birds {
    public Phoenix(String type) {
        super(type);
    }
}
