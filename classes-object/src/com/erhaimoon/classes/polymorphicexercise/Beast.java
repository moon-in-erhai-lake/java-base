package com.erhaimoon.classes.polymorphicexercise;

/**
 * @program: java-base
 * @description: 禽兽类
 * @author: erhaimoon
 * @create: 2020-12-29 21:37
 **/
public class Beast extends Animal {
    public Beast(String type) {
        super(type);
    }

    @Override
    public void eat(String food) {
        super.eat(food);
        System.out.println("在喂食的食槽里用嘴吃");
    }
}
