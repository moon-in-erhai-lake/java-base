package com.erhaimoon.classes.polymorphicexercise;

/**
 * @program: java-base
 * @description: 鸟的子类
 * @author: erhaimoon
 * @create: 2020-12-29 21:43
 **/
public class Eagle extends Birds {
    public Eagle(String type) {
        super(type);
    }
}
