package com.erhaimoon.classes.polymorphicexercise;

import java.util.Arrays;

/**
 * @program: java-base
 * @description: 给动物排序
 * @author: erhaimoon
 * @create: 2020-12-29 21:59
 **/
public class SortAnimalTest {
    public static void main(String[] args) {
        Animal[] animals = new Animal[5];
        animals[0] = new Animal("鹰", 10);
        animals[1] = new Animal("兔子", 2);
        animals[2] = new Animal("乌龟", 68);
        animals[3] = new Animal("老虎", 23);
        animals[4] = new Animal("熊", 18);
        System.out.println("排序前的动物们：");
        System.out.println(Arrays.toString(animals));
        System.out.println("排序后的动物们：");
        for (int i = 0; i < animals.length; i++) {
            for (int j = 0; j < animals.length - 1 - i; j++) {
                Animal a = animals[j];
                Animal b = animals[j + 1];
                if (a.age > b.age) {
                    animals[j + 1] = a;
                    animals[j] = b;
                }
            }
        }
        System.out.println(Arrays.toString(animals));
    }
}
