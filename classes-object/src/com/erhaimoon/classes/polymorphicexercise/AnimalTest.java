package com.erhaimoon.classes.polymorphicexercise;

/**
 * @program: java-base
 * @description: 动物测试类
 * @author: erhaimoon
 * @create: 2020-12-29 21:46
 **/
public class AnimalTest {
    public static void main(String[] args) {
        Animal a = null;
        a = new Phoenix("鸟类-凤凰");
        System.out.println(a.toString());
        a.eat("神仙饭");
        a = new Eagle("鸟类-鹰");
        System.out.println(a.toString());
        a.eat("虫子");
        a = new Bear("禽兽类-熊");
        System.out.println(a.toString());
        a.eat("蜂蜜");
        a = new Lion("兽类-狮子");
        System.out.println(a.toString());
        a.eat("鹿肉");
    }
}
