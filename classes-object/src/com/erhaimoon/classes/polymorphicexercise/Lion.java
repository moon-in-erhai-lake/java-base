package com.erhaimoon.classes.polymorphicexercise;

/**
 * @program: java-base
 * @description: 狮子类
 * @author: erhaimoon
 * @create: 2020-12-29 21:45
 **/
public class Lion extends Beast {
    public Lion(String type) {
        super(type);
    }
}
